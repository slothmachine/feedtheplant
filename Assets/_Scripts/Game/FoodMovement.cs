﻿using UnityEngine;

public class FoodMovement : MonoBehaviour {
	public float disableTime = 20;
	public float speed = 1;

	private void Start()
	{
		Destroy(gameObject, disableTime);
	}

	private void Update()
	{
		transform.Translate(transform.right * speed * Time.deltaTime);
	}
}
