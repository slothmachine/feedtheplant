﻿using UnityEngine;

public class Food : MonoBehaviour {
	public string type;
	public Sprite icon;
	public Color color;
	public bool isEdible;
	public int points;
	public int tier;

	public void Eat()
	{
		Destroy(gameObject);
	}
}
