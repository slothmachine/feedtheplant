﻿using UnityEngine;
using System.Collections.Generic;

public class AreaAim : MonoBehaviour {
	public static List<Food> targetedFoods;

	private void Awake()
	{
		targetedFoods = new List<Food>();
	}

	private void OnTriggerEnter2D(Collider2D other)
	{
		Food f = other.gameObject.GetComponent<Food>();
		if (f != null)
		{
			targetedFoods.Add(f);
		}
	}
	private void OnTriggerExit2D(Collider2D other)
	{
		Food f = other.gameObject.GetComponent<Food>();
		if (f != null)
		{
			targetedFoods.Remove(f);
		}
	}
}
