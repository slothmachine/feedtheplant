﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Plant : MonoBehaviour {
	[HideInInspector]
	public List<Food> foods = new List<Food>();

	public ParticleSystem eatParticle;
	public Color[] eatColors;
	public int growthLevel = 0;

	public AudioSource eatSource;
	public AudioSource growSource;
	public Animator animator;
	private float scaleFactor = 2;

	public void Eat(Food food)
	{
		foods.Add(food);

		eatParticle.startColor = food.color;
		eatParticle.Play();
		eatSource.pitch = 0.7f + (food.tier * 0.1f);
		eatSource.Play();

		animator.Play("Eat");
	}

	public void Grow()
	{
		StartCoroutine(GrowRoutine());
	}

	private IEnumerator GrowRoutine()
	{
		growthLevel++;
		animator.Play("Grow");
		Vector3 targetScale = transform.lossyScale * 1.25f;

		yield return new WaitForSeconds(0.8f);
		growSource.Play();
		while (transform.lossyScale.x < targetScale.x)
		{
			transform.localScale += transform.lossyScale * (scaleFactor * Time.fixedDeltaTime);

			yield return new WaitForFixedUpdate();
		}
	}
}
