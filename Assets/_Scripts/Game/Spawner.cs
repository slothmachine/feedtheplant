﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Spawner : MonoBehaviour {
	public float spawnRate = 1f;
	public float foodSpeed;
	public float disableTime;

	public List<GameObject> tierZeroFoods = new List<GameObject>();
	public List<GameObject> tierOneFoods = new List<GameObject>();
	public List<GameObject> tierTwoFoods = new List<GameObject>();
	public GameObject specialFood;

	private GameManager refManager;

	private void Start()
	{
		refManager = FindObjectOfType<GameManager>();
		StartCoroutine(SpawnFood());
	}

	private IEnumerator SpawnFood()
	{
		GameObject spawnedFood;
		while (refManager.Timer > 0)
		{
			spawnedFood = RandomizeFood(tierZeroFoods, tierOneFoods, tierTwoFoods);
			GameObject g = Instantiate(spawnedFood, transform.position, transform.rotation) as GameObject;
			g.GetComponent<FoodMovement>().speed = foodSpeed;
			g.GetComponent<FoodMovement>().disableTime = disableTime;

			yield return new WaitForSeconds(spawnRate);
		}
	}

	private GameObject RandomizeFood(List<GameObject> foods0, List<GameObject> foods1, List<GameObject> foods2)
	{
		GameObject result;
		int rand1 = Random.Range(0, 50);
		int rand2 = Random.Range(0, 50);

		if (rand1 + rand2 < 20)
		{
			result = foods0[Random.Range(0, tierZeroFoods.Count)];
		}
		else if (rand1 + rand2 < 70)
		{
			result = foods1[Random.Range(0, tierOneFoods.Count)];
		}
		else if (rand1 + rand2 < 99)
		{
			result = foods2[Random.Range(0, tierTwoFoods.Count)];
		}
		else
		{
			result = specialFood;
		}
		
		return result;
		//foodImage.sprite = currentFood.icon;
	}
}
