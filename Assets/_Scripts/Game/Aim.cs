﻿using UnityEngine;

public class Aim : MonoBehaviour {
	public static GameObject hit;

	private void OnTriggerEnter2D(Collider2D other)
	{
		hit = other.gameObject;
	}
	private void OnTriggerExit2D(Collider2D other)
	{
		hit = null;
	}
}
