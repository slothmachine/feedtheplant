﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections;
using System;

public class GameManager : MonoBehaviour {
	// game shit
	[Header("GameRun")]
	public Plant plant;

	public GameObject aim;
	public Vector3 startPos; 
	float limitPos = -5;
	bool isAiming = false;

	public GameObject areaAim;

	// score shit
	private int score;

	// combo shit
	[Header("HUD")]
	public Image[] lastFoodsDisplay;
	public Sprite blankFood;
	private List<Food> lastFoods = new List<Food>();

	public Text multiplierDisplay;
	public Text scoreDisplay;

	public Color[] comboColors;
	private Food comboFood = null;
	private int comboCounter = 1;

	// timer shit
	public Text timeDisplay;
	private int timer;
	public int Timer
	{
		get
		{
			return timer;
		}
	}
	private const int MAX_TIME = 60;
	private float aimSpeed = -2f;
	private float aimAccel = 0.2f;
	private bool control;

	private void Awake()
	{
		Init();
	}

	private void Init()
	{ 
		score = 0;
		lastFoods.Clear();
		aim.SetActive(false);
		areaAim.SetActive(false);

		timer = MAX_TIME;
		StartCoroutine(DisplayTime());
		control = true;
	}

	private IEnumerator DisplayTime()
	{
		while (timer >= 0)
		{
			timeDisplay.text = timer.ToString();
			yield return new WaitForSeconds(1.0f);
			timer -= 1;
		}

		GameOver();
		yield return null;
	}

	private void ResetCombo()
	{
		comboCounter = 1;
		multiplierDisplay.text = comboCounter.ToString() + "x";
		comboFood = null;

		DisplayCombo();
	}

	private void UpdateCombo(Food food)
	{
		// resets combo
		if (!food.isEdible)
		{
			ResetCombo();
			lastFoods.Clear();
			DisplayLastFoods();
			return;
		}
		if (comboCounter > 1 && food.type != comboFood.type)
		{
			ResetCombo();
			lastFoods.Clear();
			DisplayLastFoods();
		}

		// maintains combo size <= 3
		if (lastFoods.Count > 2)
		{
			lastFoods.RemoveAt(2);
		}

		lastFoods.Add(food);
		DisplayLastFoods();

		// checks if combo
		if (lastFoods.Count == 2)
		{
			if (!CompareFoods())
			{
				ResetCombo();
				lastFoods.RemoveAt(0);
				DisplayLastFoods();
			}
		}
		else if (lastFoods.Count == 3)
		{
			if (CompareFoods())
			{
				if (comboCounter < 3) comboCounter += 1;
				DisplayCombo();
				lastFoods.Clear();
				comboFood = food;
			}
			else
			{
				ResetCombo();
				lastFoods.Clear();
				DisplayLastFoods();
			}
		}
	}

	private void DisplayCombo()
	{
		multiplierDisplay.fontSize = 8 + 8 * comboCounter;
		multiplierDisplay.color = comboColors[comboCounter - 1];
		multiplierDisplay.text = comboCounter.ToString() + "x";
	}

	private void DisplayLastFoods()
	{
		for (int i = 0; i < 3; i++)
		{
			if (lastFoods.Count > i) lastFoodsDisplay[i].sprite = lastFoods[i].icon;
			else lastFoodsDisplay[i].sprite = blankFood;
        }
	}

	private bool CompareFoods()
	{
		bool result = true;
		string f = comboFood != null ? comboFood.type : lastFoods[0].type;

		for (int i = 0; i < lastFoods.Count; i++)
		{
			if (f != lastFoods[i].type)
			{
				result = false;
			}
		}

		return result;
	}

	private void GameOver()
	{
		control = false;
		Application.LoadLevel("Menu");
	}

	private void Update()
	{
		if (!control) return;

		if (Input.GetButtonDown("Eat"))
		{
			isAiming = true;
			StartCoroutine(MoveAim());
		}
		if (Input.GetButton("Eat"))
		{
			isAiming = true;
		}
		if (Input.GetButtonUp("Eat"))
		{
			isAiming = false;
			StopCoroutine(MoveAim());
			aim.SetActive(false);

			if (Aim.hit != null)
			{
				Food f = Aim.hit.GetComponent<Food>();
				plant.Eat(f);
				score += f.points;
				UpdateCombo(f);
				f.Eat();

				scoreDisplay.text = score.ToString();
				CheckGrowth();
			}
		}
		
		if (comboCounter == 3)
		{
			if (Input.GetButtonDown("PowerUp"))
			{
				isAiming = true;
				StartCoroutine(AimArea());
			}
			else if (Input.GetButtonUp("PowerUp"))
			{
				isAiming = false;
				comboCounter = 1;

				if (AreaAim.targetedFoods.Count > 1)
				{
					for (int i = 0; i < AreaAim.targetedFoods.Count; i++)
					{
						plant.Eat(AreaAim.targetedFoods[i]);
						score += Mathf.Abs(AreaAim.targetedFoods[i].points);
						UpdateCombo(AreaAim.targetedFoods[i]);
						AreaAim.targetedFoods[i].Eat();
					}

					scoreDisplay.text = score.ToString();
					AreaAim.targetedFoods.Clear();
					CheckGrowth();
				}
			}
		}
	}

	private void CheckGrowth()
	{
		if (score >= 500 && plant.growthLevel == 0)
		{
			plant.Grow();
		}
		else if (score >= 1250 && plant.growthLevel == 1)
		{
			plant.Grow();
		}
		else if (score >= 2500 && plant.growthLevel == 2)
		{
			plant.Grow();
		}
		else if (score >= 5000 && plant.growthLevel == 3)
		{
			plant.Grow();
		}
	}

	private IEnumerator AimArea()
	{
		areaAim.SetActive(true);

		while (isAiming) 
		{
			yield return new WaitForFixedUpdate();
		}

		areaAim.SetActive(false);
		yield return null;
	}

	private IEnumerator MoveAim()
	{
		aim.transform.position = startPos;
		aim.SetActive(true);
		float speed = aimSpeed;

		while (aim.transform.position.y > limitPos && isAiming)
		{
			aim.transform.Translate(transform.up * speed * Time.fixedDeltaTime);
			speed -= aimAccel;
			yield return new WaitForFixedUpdate();
		}
	}
}
