﻿using UnityEngine;
using System.Collections;

public class MenuManager : MonoBehaviour {
	public GameObject menu;
	public GameObject tuto;

	void Update () {
		if (Input.GetButtonDown("Eat"))
		{
			Application.LoadLevel("Game");
		}
		if (Input.GetButtonDown("PowerUp"))
		{
			menu.SetActive(!menu.activeInHierarchy);
			tuto.SetActive(!tuto.activeInHierarchy);
		}
	}
}
